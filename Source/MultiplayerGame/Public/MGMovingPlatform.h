// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MGMovingPlatform.generated.h"

/**
 * @author Archon
 * 
 * Simple mesh which can move at defined speed 
 */
UCLASS()
class MULTIPLAYERGAME_API AMGMovingPlatform final : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	AMGMovingPlatform();

	// Centimeters per second
	UPROPERTY(EditAnywhere, Category="Movement")
	float MovementSpeed = 10;

	// Local target location
	UPROPERTY(EditAnywhere, Category="Movement",
		meta=(MakeEditWidget=true))
	FVector TargetLocation;

private:
	FVector GlobalStartLocation;
	FVector GlobalTargetLocation;

	// If 0 then platform can't move
	// Set 1 if any trigger could not stop this platform 
	UPROPERTY(EditAnywhere, Category="Trigger", meta=(ClampMin=0))
	int ActiveTriggers = 1;

protected:
	// Setting variables
	virtual void BeginPlay() override;

public:
	// Tick override
	virtual void Tick(float DeltaSeconds) override;

	// Trigger for moving this platform
	void AddActiveTrigger();
	void RemoveActiveTrigger();
};
