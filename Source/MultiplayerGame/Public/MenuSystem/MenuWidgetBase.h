// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuInterface.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidgetBase.generated.h"

/**
 * @author Archon
 */
UCLASS()
class MULTIPLAYERGAME_API UMenuWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:
	// Display Main Menu
	void Setup();
	// Destroy Main Menu
	void Teardown();

	// Set Host & Join logic by loading Main Menu in UMGPuzzleGameInstance::LoadMenu()
	void SetMenuInterface(IMenuInterface* IMenuInterface);

protected:
	// Menu logic
	IMenuInterface* MenuInterface;
};
