// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidgetBase.h"
#include "Blueprint/UserWidget.h"
#include "MainMenu.generated.h"

class UEditableTextBox;
class UWidgetSwitcher;
class UButton;

/**
 * @author Archon
 *
 * Widget Blueprint button must named exactly like in the header file 
 */
UCLASS()
class MULTIPLAYERGAME_API UMainMenu final : public UMenuWidgetBase
{
	GENERATED_BODY()

	// Main menu button
	UPROPERTY(meta=(BindWidget))
	UButton* HostButton;

	// Main menu button
	UPROPERTY(meta=(BindWidget))
	UButton* JoinButton;

	// Main menu button
	UPROPERTY(meta=(BindWidget))
	UButton* ExitButton;

	// Main menu switcher
	UPROPERTY(meta=(BindWidget))
	UWidgetSwitcher* MenuSwitcher;

	// Main sub menu overlay
	UPROPERTY(meta=(BindWidget))
	UWidget* MainMenuOverlay;

	// Join sub menu overlay
	UPROPERTY(meta=(BindWidget))
	UWidget* JoinMenuOverlay;

	// Join menu ip address text box
	UPROPERTY(meta=(BindWidget))
	UEditableTextBox* IpAddressField;

	// Join sub menu confirm button
	UPROPERTY(meta=(BindWidget))
	UButton* JoinServerButton;
	
	// Join sub menu cancel button
	UPROPERTY(meta=(BindWidget))
	UButton* JoinMenuCancelButton;

protected:
	// Setting delegates
	virtual bool Initialize() override;

private:
	// HostButton delegate
	UFUNCTION()
	void HostServer();

	// Join sub menu connect to server button
	UFUNCTION()
	void JoinServer();

	// JoinButton delegate
	UFUNCTION()
	void OpenJoinMenu();

	// JoinMenuCancelButton delegate
	UFUNCTION()
	void OpenMainMenu();

	// Validate and set widget menu
	void SetActiveMenuWidget(UWidget* Widget) const;

	// ExitButton delegate
	UFUNCTION()
	void ExitGame();
};
