// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidgetBase.h"
#include "InGameMenu.generated.h"

class UButton;

/**
 * @author Archon
 */
UCLASS()
class MULTIPLAYERGAME_API UInGameMenu final : public UMenuWidgetBase
{
	GENERATED_BODY()

	UPROPERTY(meta=(BindWidget))
	UButton* ReturnButton;

	UPROPERTY(meta=(BindWidget))
	UButton* QuitToMenuButton;

	UPROPERTY(meta=(BindWidget))
	UButton* QuitToDesktopButton;
	
protected:
	virtual bool Initialize() override;

private:
	UFUNCTION()
	void ReturnToGame();

	UFUNCTION()
	void QuitToMainMenu();

	UFUNCTION()
	void QuitToDesktop();
};
