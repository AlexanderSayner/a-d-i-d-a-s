// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MenuSystem/MenuInterface.h"
#include "MGPuzzleGameInstance.generated.h"

class UInGameMenu;
class UMainMenu;
class UUserWidget;

/**
 * @author Archon
 *
 * Console commands
 */
UCLASS()
class MULTIPLAYERGAME_API UMGPuzzleGameInstance final : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

public:
	explicit UMGPuzzleGameInstance(const FObjectInitializer& ObjectInitializer);
	
private:
	TSubclassOf<UUserWidget> MenuClass;
	TSubclassOf<UUserWidget> InGameMenuClass;

	// https://forums.unrealengine.com/t/got-a-little-confused-about-garbage-collection/85154
	UPROPERTY()
	UMainMenu* Menu;
	UPROPERTY()
	UInGameMenu* EscMenu;

public:
	// UGameInstance
	virtual void Init() override;

	// 'Host' a server 
	UFUNCTION(Exec)
	virtual void Host() override;

	// 'Join' to a server 
	UFUNCTION(Exec)
	virtual void Join(const FString& Address) override;

	// Called by actor's begin play on blackout level
	UFUNCTION(BlueprintCallable)
	void LoadMenu();

	// Called by Player Controller
	UFUNCTION(BlueprintCallable)
	void LoadInGameMenu();
};
