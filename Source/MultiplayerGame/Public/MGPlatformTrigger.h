// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MGPlatformTrigger.generated.h"

class UBoxComponent;
class AMGMovingPlatform;

/**
 * @author Archon
 *
 * Pressure platform trigger
 */
UCLASS()
class MULTIPLAYERGAME_API AMGPlatformTrigger final : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMGPlatformTrigger();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere)
	UBoxComponent* TriggerVolume;

	// Moving platforms affected by this trigger
	UPROPERTY(EditAnywhere, Category="Trigger")
	TArray<AMGMovingPlatform*> MovingPlatforms;

	// Dynamic event requires Unreal FUNCTION macro
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                    int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Dynamic event requires Unreal FUNCTION macro
	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                  int32 OtherBodyIndex);
};
