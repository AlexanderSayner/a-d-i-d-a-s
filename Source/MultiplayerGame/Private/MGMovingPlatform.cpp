// Fill out your copyright notice in the Description page of Project Settings.


#include "MGMovingPlatform.h"

// Default constructor
AMGMovingPlatform::AMGMovingPlatform()
{
	PrimaryActorTick.bCanEverTick = true;

	SetMobility(EComponentMobility::Movable);
}

// Synchronize movement with clients
void AMGMovingPlatform::BeginPlay()
{
	Super::BeginPlay();

	// Only server can replicate
	if (HasAuthority())
	{
		SetReplicates(true);
		SetReplicateMovement(true);
	}

	GlobalStartLocation = GetActorLocation();
	// Transform from local to global
	GlobalTargetLocation = GetTransform().TransformPosition(TargetLocation);
}

// Moving every tick
void AMGMovingPlatform::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// Moving only on server
	if (HasAuthority() && ActiveTriggers > 0)
	{
		FVector Location = GetActorLocation();
		// Lenght from A to B
		const float JourneyLength = (GlobalTargetLocation - GlobalStartLocation).Size();
		const float JourneyTravelled = (Location - GlobalStartLocation).Size();
		// if the journey travelled is longer then the journey length when platform has reached its destination
		if (JourneyTravelled >= JourneyLength)
		{
			// Go in the opposite way
			const FVector Swap = GlobalStartLocation;
			GlobalStartLocation = GlobalTargetLocation;
			GlobalTargetLocation = Swap;
		}
		const FVector Direction = (GlobalTargetLocation - GlobalStartLocation).GetSafeNormal();
		Location += MovementSpeed * DeltaSeconds * Direction;
		SetActorLocation(Location);
	}
}

// Remember all platforms which are pressed and can move this platform 
void AMGMovingPlatform::AddActiveTrigger()
{
	ActiveTriggers++;
}

// Remove platform which is not pressing anymore
void AMGMovingPlatform::RemoveActiveTrigger()
{
	if (ActiveTriggers > 0)
	{
		ActiveTriggers--;
	}
}
