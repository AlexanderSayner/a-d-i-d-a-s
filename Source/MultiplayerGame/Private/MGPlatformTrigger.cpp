// Fill out your copyright notice in the Description page of Project Settings.


#include "MGPlatformTrigger.h"

#include "MGMovingPlatform.h"
#include "Components/BoxComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogPlatformTrigger, All, All);

// Sets default values
AMGPlatformTrigger::AMGPlatformTrigger()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set subobjects
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume"));
	// Set root component
	RootComponent = TriggerVolume;
}

// Called when the game starts or when spawned
void AMGPlatformTrigger::BeginPlay()
{
	Super::BeginPlay();

	// Checking for null only in debug and development mode
	check(TriggerVolume)

	// Set events
	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AMGPlatformTrigger::OnBeginOverlap);
	TriggerVolume->OnComponentEndOverlap.AddDynamic(this, &AMGPlatformTrigger::OnEndOverlap);
}

// Called every frame
void AMGPlatformTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Dynamic event couldn't be static
// ReSharper disable once CppMemberFunctionMayBeStatic 
void AMGPlatformTrigger::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
                                        UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                        const FHitResult& SweepResult)
{
	UE_LOG(LogPlatformTrigger, Display, TEXT("Activated"))
	for (AMGMovingPlatform* Platform : MovingPlatforms)
	{
		Platform->AddActiveTrigger();
	}
}

// Dynamic event couldn't be static
// ReSharper disable once CppMemberFunctionMayBeStatic
void AMGPlatformTrigger::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                      UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogPlatformTrigger, Display, TEXT("Deactivated"))
	for (AMGMovingPlatform* Platform : MovingPlatforms)
	{
		Platform->RemoveActiveTrigger();
	}
}
