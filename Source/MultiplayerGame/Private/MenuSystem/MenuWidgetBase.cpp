// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSystem/MenuWidgetBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogMenuWidgetBase, All, All);

// Add this menu to a viewport
void UMenuWidgetBase::Setup()
{
	// Display MainMenu
	this->AddToViewport();

	const UWorld* World = GetWorld();
	if (World == nullptr)
	{
		UE_LOG(LogMenuWidgetBase, Error, TEXT("Not valid UWorld"))
		return;
	}

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (PlayerController == nullptr)
	{
		UE_LOG(LogMenuWidgetBase, Error, TEXT("PlayerController is not valid"))
		return;
	}

	// Make player able to use mouse for a click
	FInputModeUIOnly InputModeData;
	this->bIsFocusable = true;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = true;
}

// Close main menu and
void UMenuWidgetBase::Teardown()
{
	UE_LOG(LogMenuWidgetBase, Display, TEXT("Menu Teard down called"));

	if (this->IsInViewport())
	{
		this->RemoveFromViewport();
	}

	const UWorld* World = GetWorld();
	if (World == nullptr)
	{
		UE_LOG(LogMenuWidgetBase, Error, TEXT("Not valid UWorld"))
		return;
	}

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (PlayerController == nullptr)
	{
		UE_LOG(LogMenuWidgetBase, Error, TEXT("PlayerController is not valid"))
		return;
	}

	// Default input mode
	const FInputModeGameOnly InputModeData;
	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = false;
}

// Dependency Injection
void UMenuWidgetBase::SetMenuInterface(IMenuInterface* IMenuInterface)
{
	this->MenuInterface = IMenuInterface;
}
