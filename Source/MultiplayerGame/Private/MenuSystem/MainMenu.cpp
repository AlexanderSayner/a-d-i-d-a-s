// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSystem/MainMenu.h"

#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/WidgetSwitcher.h"

DEFINE_LOG_CATEGORY_STATIC(LogMainMenuUserWidget, All, All);

// Binding button events
bool UMainMenu::Initialize()
{
	// Initialize
	const bool bIsSuccess = Super::Initialize();
	if (!bIsSuccess)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("Could not initialize Main Menu widget"))
		return false;
	}

	// HostButton
	if (HostButton == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("Host button is not initialised"))
		return false;
	}
	HostButton->OnClicked.AddDynamic(this, &UMainMenu::HostServer);

	// JoinButton
	if (JoinButton == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("Join button is not initialised"))
		return false;
	}
	JoinButton->OnClicked.AddDynamic(this, &UMainMenu::OpenJoinMenu);

	// CancelButton
	if (JoinMenuCancelButton == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("Join menu cancel button is not initialised"))
		return false;
	}
	JoinMenuCancelButton->OnClicked.AddDynamic(this, &UMainMenu::OpenMainMenu);

	// JoinServerButton
	if (JoinServerButton == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("Join server button is not initialised"))
		return false;
	}
	JoinServerButton->OnClicked.AddDynamic(this, &UMainMenu::JoinServer);

	// ExitButton
	if (ExitButton == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("Exit button is not initialised"))
		return false;
	}
	ExitButton->OnClicked.AddDynamic(this, &UMainMenu::ExitGame);

	return true;
}

// Host server from Main Menu widget
// Could not be const or static when using in delegate
// ReSharper disable once CppMemberFunctionMayBeStatic
// ReSharper disable once CppMemberFunctionMayBeConst
void UMainMenu::HostServer()
{
	if (MenuInterface == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("MenuInterface logic object is not initialized"))
		return;
	}

	// Host logic realisation
	MenuInterface->Host();

	UE_LOG(LogMainMenuUserWidget, Display, TEXT("Hosting server"))
}

// Join server event from Main Menu widget
// ReSharper disable once CppMemberFunctionMayBeStatic
// ReSharper disable once CppMemberFunctionMayBeConst
void UMainMenu::JoinServer()
{
	if (MenuInterface == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("MenuInterface logic object is not initialized"))
		return;
	}
	if (IpAddressField == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("IpAddressField is not initialized"))
		return;
	}
	
	// Text field value
	const FString& Address = IpAddressField->GetText().ToString();
	// Join logic realisation
	MenuInterface->Join(Address);

	UE_LOG(LogMainMenuUserWidget, Display, TEXT("Joining server"))
}

// Join Menu SubMenu
// ReSharper disable once CppMemberFunctionMayBeConst
void UMainMenu::OpenJoinMenu()
{
	this->SetActiveMenuWidget(JoinMenuOverlay);
}

// Return to the main menu
// ReSharper disable once CppMemberFunctionMayBeConst
void UMainMenu::OpenMainMenu()
{
	this->SetActiveMenuWidget(MainMenuOverlay);
}

// Menu Switcher
void UMainMenu::SetActiveMenuWidget(UWidget* Widget) const
{
	if (MenuSwitcher == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("MenuSwitcher is not initialized"))
		return;
	}

	if (JoinMenuOverlay == nullptr)
	{
		UE_LOG(LogMainMenuUserWidget, Error, TEXT("JoinMenu is not initialized"))
		return;
	}

	MenuSwitcher->SetActiveWidget(Widget);
}

// Exit from game
// ReSharper disable once CppMemberFunctionMayBeStatic
// ReSharper disable once CppMemberFunctionMayBeConst
void UMainMenu::ExitGame()
{
	const UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	PlayerController->ConsoleCommand("quit");
}
