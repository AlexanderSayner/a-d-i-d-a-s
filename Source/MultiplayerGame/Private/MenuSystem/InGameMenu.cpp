// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSystem/InGameMenu.h"

#include "Components/Button.h"

DEFINE_LOG_CATEGORY_STATIC(LogInGameMenuUserWidget, All, All);

// Binding button events
bool UInGameMenu::Initialize()
{
	// Initialize
	const bool bIsSuccess = Super::Initialize();
	if (!bIsSuccess)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Could not initialize In Game Menu widget"))
		return false;
	}

	// ReturnButton
	if (ReturnButton == nullptr)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Return button is not initialised"))
		return false;
	}
	ReturnButton->OnClicked.AddDynamic(this, &UInGameMenu::ReturnToGame);

	// QuitToMenuButton
	if (QuitToMenuButton == nullptr)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Quit to menu button is not initialised"))
		return false;
	}
	QuitToMenuButton->OnClicked.AddDynamic(this, &UInGameMenu::QuitToMainMenu);

	// QuitToDesktopButton
	if (QuitToDesktopButton == nullptr)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Quit game button is not initialised"))
		return false;
	}
	QuitToDesktopButton->OnClicked.AddDynamic(this, &UInGameMenu::QuitToDesktop);

	return true;
}

// Close this menu 
void UInGameMenu::ReturnToGame()
{
	this->Teardown();
}

// Open main menu level
void UInGameMenu::QuitToMainMenu()
{
	if (MenuInterface == nullptr)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Lodgic menu core is not initialised"))
		return;
	}

	const UWorld* World = GetWorld();
	if (World == nullptr)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Could not get UWorld"))
		return;
	}

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (PlayerController == nullptr)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Could not get Player Controiller"))
		return;
	}

	// Move player to the main menu map 
	this->Teardown();
	PlayerController->ClientTravel("/Game/MultiplayerGamePuzzle/Maps/MainMenuScreen", TRAVEL_Absolute);
}

// Quit from game
// ReSharper disable once CppMemberFunctionMayBeConst
void UInGameMenu::QuitToDesktop()
{
	const UWorld* World = GetWorld();
	if (World == nullptr)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Could not get UWorld"))
		return;
	}

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (PlayerController == nullptr)
	{
		UE_LOG(LogInGameMenuUserWidget, Error, TEXT("Could not get Player Controiller"))
		return;
	}

	// Quit command
	PlayerController->ConsoleCommand("quit");
}
