// Fill out your copyright notice in the Description page of Project Settings.


#include "MGPuzzleGameInstance.h"

#include "MGPlatformTrigger.h"
#include "Blueprint/UserWidget.h"
#include "MenuSystem/InGameMenu.h"
#include "MenuSystem/MainMenu.h"

DEFINE_LOG_CATEGORY_STATIC(LogPuzzleGameInstance, All, All);

// Find Classes
UMGPuzzleGameInstance::UMGPuzzleGameInstance(const FObjectInitializer& ObjectInitializer)
{
	UE_LOG(LogPuzzleGameInstance, Display, TEXT("Constructor of Puzzle Game Instance"))

	const ConstructorHelpers::FClassFinder<AMGPlatformTrigger> PlatformTriggerBPClass(
		TEXT("/Game/MultiplayerGamePuzzle/BP_PlatformTrigger"));

	if (PlatformTriggerBPClass.Class == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("PlatformTriggerBPClass.Class is not valid"))
		return;
	}

	UE_LOG(LogPuzzleGameInstance, Display, TEXT("Found class %s"), *PlatformTriggerBPClass.Class->GetName())

	// MenuClass
	const ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(
		TEXT("/Game/MultiplayerGamePuzzle/MenuSystem/WBP_MainMenu"));

	if (MenuBPClass.Class == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("WBP_MainMenu.Class is not valid"))
		return;
	}

	MenuClass = MenuBPClass.Class;
	UE_LOG(LogPuzzleGameInstance, Display, TEXT("Found class %s"), *MenuBPClass.Class->GetName())

	// InGameMenuClass
	const ConstructorHelpers::FClassFinder<UUserWidget> InGameMenuBPClass(
		TEXT("/Game/MultiplayerGamePuzzle/MenuSystem/WBP_EscMenu"));

	if (InGameMenuBPClass.Class == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("WBP_EscMenu.Class is not valid"))
		return;
	}

	InGameMenuClass = InGameMenuBPClass.Class;
	UE_LOG(LogPuzzleGameInstance, Display, TEXT("Found class %s"), *InGameMenuBPClass.Class->GetName())
}

// Dummy
void UMGPuzzleGameInstance::Init()
{
	Super::Init();

	UE_LOG(LogPuzzleGameInstance, Display, TEXT("Init Method of Puzzle Game Instance"))
}

// Server side travel and listen
void UMGPuzzleGameInstance::Host()
{
	UE_LOG(LogPuzzleGameInstance, Display, TEXT("Host Method of Puzzle Game Instance"))

	// UEngine* Engine = GetEngine();
	if (GEngine == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("Not valid GEngine"))
		return;
	}

	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("Not valid UWorld"))
		return;
	}

	if (Menu == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("Menu is not valid"))
	}
	else
	{
		UE_LOG(LogPuzzleGameInstance, Display, TEXT("Calling menu teardown"))
		Menu->Teardown();
	}

	// Print String
	GEngine->AddOnScreenDebugMessage(0, 5, FColor::Green,TEXT("Hosting"));

	// Move to another map
	World->ServerTravel("/Game/ThirdPersonBP/Maps/ThirdPersonExampleMap?listen");
}

// Client side travel
void UMGPuzzleGameInstance::Join(const FString& Address)
{
	UE_LOG(LogPuzzleGameInstance, Display, TEXT("Join Method of Puzzle Game Instance"))

	if (GEngine == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("Not valid GEngine"))
		return;
	}

	// Get Player Controller
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (PlayerController == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("PlayerController is not valid"))
		return;
	}

	if (Menu == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("Menu is not valid"))
	}
	else
	{
		Menu->Teardown();
	}

	GEngine->AddOnScreenDebugMessage(0, 5, FColor::Green, FString::Printf(TEXT("Joining to %s"), *Address));

	// Connect to a host
	PlayerController->ClientTravel(Address, TRAVEL_Absolute);
}

// Show main menu widget
void UMGPuzzleGameInstance::LoadMenu()
{
	if (MenuClass == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("There was an error in initialising WBP_MainMenu.Class"))
		return;
	}

	// Create menu widget
	Menu = CreateWidget<UMainMenu>(this, MenuClass);

	if (Menu == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("Menu widget was not created"))
		return;
	}

	Menu->SetMenuInterface(this);

	// Add menu to a viewport
	Menu->Setup();
}

// Show esc menu
void UMGPuzzleGameInstance::LoadInGameMenu()
{
	if (InGameMenuClass == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("InGameMenuClass is not initialised"))
		return;
	}

	// Create esc menu widget
	EscMenu = CreateWidget<UInGameMenu>(this, InGameMenuClass);
	if (EscMenu == nullptr)
	{
		UE_LOG(LogPuzzleGameInstance, Error, TEXT("InGameMenu widget was not created"))
		return;
	}

	EscMenu->SetMenuInterface(this);
	EscMenu->Setup();
}
