#Multiplayer
Start listen server  
```shell
./UE4Editor ~/UnrealProjects/MultiplayerGame/MultiplayerGame.uproject /Game/ThirdPersonBP/Maps/ThirdPersonExampleMap?listen -server -log
```

Connect to server  
```shell
~/UnrealEngine/Engine/Binaries/Linux/UE4Editor ~/UnrealProjects/MultiplayerGame/MultiplayerGame.uproject 192.168.1.7 -game -log
```

Connect to server  
```shell
MyGame.exe 127.0.0.1
```

##Multiplayer mode
Start a game with default map (might not be default - doesn't matter)
```shell
~/UnrealEngine/Engine/Binaries/Linux/UE4Editor ~/UnrealProjects/MultiplayerGame/MultiplayerGame.uproject -game -log
```

Console command to host a server  
*Host*  
Console command to join a server  
*Join 127.0.0.1*  
